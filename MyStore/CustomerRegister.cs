namespace MyStore
{
    public class CustomerRegister
    {
        private List<Customer> _customers { get; }


        // Moderniserad konstruktor - går även att göra om till primär konstruktor för snyggare kod
        public CustomerRegister() => _customers = new List<Customer>();

        // "Gamla" konstruktorn
        // public CustomerRegister()
        // {
        //     _customers = new List<Customer>();
        // }

        // public IEnumerable<Customer> CustomerList => Customers.AsReadOnly();
        public IReadOnlyList<Customer> CustomerList => _customers.AsReadOnly();

        // Ändrat syntax - mindre kod - tidigare kod utkommenterad
        public void AddCustomer(Customer customer) => _customers.Add(customer);
        // public void AddCustomer(Customer customer)
        // {
        //     _customers.Add(customer);
        // }


        // Ändrat syntax - mindre kod - tidigare kod utkommenterad
        public Customer? FindCustomerByEmail(string email) => _customers.Find(c => c.Email == email);

        // public Customer? FindCustomerByEmail(string email)
        // {
        //     return _customers.Find(c => c.Email == email);
        // }

        // Första funktion för att lista alla kunder - Använt interface ReadOnlyList för bättre inkapsling
        // public List<Customer> GetAllCustomers()
        // {
        //     return Customers;
        // }


    }
}