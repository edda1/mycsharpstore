namespace MyStore
{
    public record Address(string Street, string City, string PostalCode);
}