namespace MyStore
{
    public class Electronic : Product
    {
        // Fält
        public string Brand { get; private set; }

        // Konstruktor
         public Electronic(string name, decimal price, string brand)
        : base(name, price) => Brand = brand ?? throw new ArgumentException("Brand cannot be null or empty.");

        // Tidigare syntax
        // public Electronic(string name, decimal price, string brand)
        // : base(name, price)
        // {
        //     if (string.IsNullOrWhiteSpace(brand))
        //         throw new ArgumentException("Brand cannot be null or empty");
        //     Brand = brand;
        // }

        // Implementering av den abstrakta metoden DisplayInfo
        public override void DisplayInfo() => Console.WriteLine($"Elektronisk Produkt: {Name}, Pris: {Price:C}, Varumärke: {Brand}");

        // Tidigare syntax
        // public override void DisplayInfo()
        // {
        //     System.Console.WriteLine($"Elektronik: {Name}, Pris: {Price:C}, Varumärke: {Brand}");
        // }




    }
}