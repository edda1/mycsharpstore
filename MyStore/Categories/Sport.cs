using MyStore;

namespace MyStore
{
    public class Sport : Product
    {
        public string SportType { get; private set; }

        public Sport(string name, decimal price, string sportType)
        : base(name, price) => SportType = sportType ?? throw new ArgumentException("Sport type cannot be null or empty.");

        public override void DisplayInfo() => Console.WriteLine($"Sportprodukt: {Name}, Pris: {Price:C}, Sporttyp: {SportType}");
   
    }
}