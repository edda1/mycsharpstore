using MyStore;

namespace MyStore
{
    public class Poster : Product
    {
        public string Size { get; private set; }

        public Poster(string name, decimal price, string size)
        : base(name, price) => Size = size ?? throw new ArgumentException("Size cannot be null or empty.");

        public override void DisplayInfo() => Console.WriteLine($"Affisch: {Name}, Pris: {Price:C}, Storlek: {Size}");
    }
}