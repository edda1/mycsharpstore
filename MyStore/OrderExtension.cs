namespace MyStore 
{
    public static class OrderExtension
    {
        public static decimal GetTotalCost(this Order order) => order.Products.Sum(product => product.Price);

    }
    
}