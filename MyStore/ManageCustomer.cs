using System.Globalization;

namespace MyStore
{
    public class ManageCustomer
    {
        private readonly CustomerRegister _customerRegister;

        // Moderniserad konstruktor - går även att göra om till primär konstruktor för snyggare kod
        public ManageCustomer(CustomerRegister customerRegister) => _customerRegister = customerRegister;

        // Tidigare konstruktor
        // public ManageCustomer(CustomerRegister customerRegister)
        // {
        //     _customerRegister = customerRegister;
        // }

        public void AddCustomer()
        {
            System.Console.WriteLine("Ange kundens namn: ");
            string? name = Console.ReadLine();
            System.Console.WriteLine("Ange kundens email: ");
            string? email = Console.ReadLine();
            System.Console.WriteLine("Ange kundens gatuadress: ");
            string? street = Console.ReadLine();
            System.Console.WriteLine("Ange kundens stad: ");
            string? city = Console.ReadLine();
            System.Console.WriteLine("Ange kundens postnummer: ");
            string? postalCode = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(email) || string.IsNullOrWhiteSpace(street) || string.IsNullOrWhiteSpace(city) || string.IsNullOrWhiteSpace(postalCode))
            {
                System.Console.WriteLine("Alla fält måste fyllas i.");
                return;
            }

            // Både new Address och new Customer kan ändras till endast new-syntax
            Address address = new Address(street, city, postalCode);
            Customer customer = new Customer(name, email, address);
            _customerRegister.AddCustomer(customer);
            System.Console.WriteLine("Kund tillagd.");
            // DisplayAllCustomers(customerRegister); Användes initialt i Program.cs 

        }
        public void AddCustomerOrder()
        {
            System.Console.WriteLine("Ange kundens email: ");
            string? email = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(email))
            {
                System.Console.WriteLine("Du måste ange en giltig email.");
                return;
            }

            Customer? customer = _customerRegister.FindCustomerByEmail(email);

            if (customer == null)
            {
                System.Console.WriteLine("Kunden hittades inte.");
                return;
            }

            Order order = CreateNewOrder();
            customer.AddOrder(order);
            System.Console.WriteLine("Ordern har lagts till");
        }
        private Order CreateNewOrder()
        {
            System.Console.WriteLine("Ange order ID");
            string? orderIdString = Console.ReadLine();

            if (!int.TryParse(orderIdString, out int orderId))
            {
                System.Console.WriteLine("Ogiltigt order ID");
                throw new ArgumentException("Order ID is not valid");
            }

            // Här kan new-syntaxen användas
            Order order = new Order(orderId, DateTime.Now);

            bool addingProducts = true;

            while (addingProducts)
            {
                Console.WriteLine("Välj kategor!");
                Console.WriteLine("1. Elektronik");
                System.Console.WriteLine("2. Sport");
                Console.WriteLine("3. Affischer");
                Console.Write("Välj ett alternativ: ");
                string? productChoice = Console.ReadLine();

                if (string.IsNullOrWhiteSpace(productChoice))
                {
                    System.Console.WriteLine("Ogiligt val, försök igen.");
                    continue;
                }

                switch (productChoice)
                {
                    case "1":
                        order.AddProduct(CreateProduct<Electronic>());
                        break;
                    case "2":
                        order.AddProduct(CreateProduct<Sport>());
                        break;
                    case "3":
                        order.AddProduct(CreateProduct<Poster>());
                        break;
                    default:
                        Console.WriteLine("Ogiltigt val, försök igen.");
                        continue;
                }
                Console.Write("Vill du lägga till fler produkter? (j/n)");
                string? addMore = Console.ReadLine()?.ToLower();

                if (addMore != "j")
                {
                    addingProducts = false;
                }
            }
            return order;
        }
        private static Product CreateProduct<T>() where T : Product
        {
            Console.Write("Ange produktens namn: ");
            string? name = Console.ReadLine();
            Console.Write("Ange pris: ");
            string? priceString = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(priceString) || !decimal.TryParse(priceString, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal price))
            {
                throw new ArgumentException("Ogiltiga produktuppgifter.");
            }

            if (typeof(T) == typeof(Electronic))
            {
                Console.Write("Ange varumärke: ");
                string? brand = Console.ReadLine();
                return new Electronic(name!, price, brand!);
            }
            else if (typeof(T) == typeof(Sport))
            {
                Console.Write("Ange sporttyp: ");
                string? sportType = Console.ReadLine();
                return new Sport(name!, price, sportType!);
            }
            else if (typeof(T) == typeof(Poster))
            {
                Console.Write("Ange storlek: ");
                string? size = Console.ReadLine();
                return new Poster(name!, price, size!);
            }

            throw new ArgumentException("Okänd kategori");
        }
        public void DisplayAllCustomers()
        {
            // foreach (var customer in customerRegister.GetAllCustomers()) - tidigare funktion för att hämta alla kunder
            foreach (var customer in _customerRegister.CustomerList)
            {
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                System.Console.WriteLine($"Kundens namn: {customer.Name}");
                System.Console.WriteLine($"Kundens stad: {customer.Address.City}");
                foreach (var order in customer.Orders)
                {
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine($"  Order ID: {order.OrderId}, Order Date: {order.OrderDate}");
                    foreach (var product in order.Products)
                    {
                        product.DisplayInfo();
                    }
                    Console.WriteLine($"Total kostnad: {order.GetTotalCost():C} \n"); // radbryt för tydligare utskrift
                }

                Console.ResetColor();
            }
        }
    }

}