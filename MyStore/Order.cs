using System.Net.Http.Headers;

namespace MyStore
{

    public class Order
    {
        public int OrderId { get; private set; }
        public DateTime OrderDate { get; private set; }

        private readonly List<Product> _products;

        public Order(int orderId, DateTime orderDate)
        {
            OrderId = orderId;
            OrderDate = orderDate;
            _products = new List<Product>();
        }

        // public IReadOnlyList<Product> Products => _products.AsReadOnly();
        public IReadOnlyList<Product> Products => _products;

        // Funktion för att lägga till en produkt till listan
        public void AddProduct(Product product) => _products.Add(product);

        // Tidigare syntax
        // public void AddProduct(Product product)
        // {
        //     _products.Add(product);
        // }

        // Tidigare funktion som returnerar en lista med produkter
        // public List<Product> GetProducts()
        // {
        //     return Products;
        // }

    }
}