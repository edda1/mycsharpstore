namespace MyStore
{
    public struct Discount
    {
        public decimal Percentage { get; }

        public Discount(decimal percentage)
        {
            if (percentage < 0 || percentage > 100)
                throw new ArgumentOutOfRangeException(nameof(percentage), "Percentage must be between 0 and 100");
        }
    }

}