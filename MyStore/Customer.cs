namespace MyStore
{
    public class Customer
    {
        public string Name { get; private set; }
        public string Email { get; private set; }
        public Address Address { get; private set; }
        private readonly List<Order> _orders;

        public Customer(string name, string email, Address address)
        {
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(email))
                throw new ArgumentException("Name and email cannot be null or empty");
            Name = name;
            Email = email;
            Address = address;
            _orders = new List<Order>();
        }

        // public IEnumerable<Order> Orders => _orders.AsReadOnly();
        public IReadOnlyList<Order> Orders => _orders.AsReadOnly();
        public void AddOrder(Order order) => _orders.Add(order);
    }
}