namespace MyStore
{
    public abstract class Product
    {
        public string Name { get; private set; }
        public decimal Price { get; private set; }

        protected Product(string name, decimal price)
        {
            if (string.IsNullOrWhiteSpace(name) || price <= 0)
            throw new ArgumentException("Name cannot be null or empty and price must be postive ");
            Name = name;
            Price = price;
        }

        // Alla klasser som ärver från Product måste ha en implementering av denna funktion
        public abstract void DisplayInfo();    
    }
}