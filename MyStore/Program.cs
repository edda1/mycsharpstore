﻿namespace MyStore;

class Program
{
    static void Main(string[] args)
    {
        CustomerRegister customerRegister = new CustomerRegister();
        ManageCustomer manageCustomer = new ManageCustomer(customerRegister);
        // AddCustomer(customerRegister); - Denna funktion är flyttad till ManageCustomer

        bool running = true;

        while (running)
        {
            Console.WriteLine("Välkommen!");
            Console.WriteLine("1. Lägg till en kund");
            System.Console.WriteLine("2. Lägg till order till kund");
            Console.WriteLine("3. Visa alla kunder");
            Console.WriteLine("4. Avsluta");
            Console.Write("Välj ett alternativ: ");
            string? choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    manageCustomer.AddCustomer();
                    break;
                case "2":
                    manageCustomer.AddCustomerOrder();
                    break;
                case "3":
                    manageCustomer.DisplayAllCustomers();
                    break;
                case "4":
                    running = false;
                    break;
                default:
                    Console.WriteLine("Ogiltigt val, försök igen.");
                    break;
            }
            Console.WriteLine();
        }
    }
}


